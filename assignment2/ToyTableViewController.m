//
//  ToyTableViewController.m
//  assignment2
//
//  Created by Devin on 2018-10-25.
//  Copyright © 2018 Devin. All rights reserved.
//

#import "ToyTableViewController.h"
#import "AddToyViewController.h"
#import "ToyDetailViewController.h"
#import "Toy.h"

@interface ToyTableViewController ()

@property NSMutableArray *toys;
@property Toy *selectedToy;

@end

@implementation ToyTableViewController

const float CELL_IMAGE_HEIGHT = 90;
const float CELL_IMAGE_WIDTH = 90;
const float CELL_TEXT_HEIGHT = 30;
const float CELL_TEXT_WIDTH = 200;
const float CELL_TEXT_OFFSET = 10;
const float CELL_TEXT_TITLE_SIZE = 20;
const float CELL_TEXT_SIZE = 16;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.toys = [[NSMutableArray alloc] init];
    [self loadInitialToys];
}

- (void)loadInitialToys {
    //Load data if it exists
    NSData *storedData = [[NSUserDefaults standardUserDefaults] objectForKey:@"toys"];
    
    if (storedData != nil) {
        _toys = [NSKeyedUnarchiver unarchiveObjectWithData:storedData];
    } else {
        //Populate default items
        Toy *xwing = [[Toy alloc] init];
        xwing.name = @"X-Wing";
        xwing.brand = @"Star Wars";
        xwing.price = 19.99;
        xwing.info = @"Star Wars X-Wing";
        xwing.image = [UIImage imageNamed:@"xwing"];
        [self.toys addObject:xwing];
        
        Toy *nerfgun = [[Toy alloc] init];
        nerfgun.name = @"Nerf Blaster";
        nerfgun.brand = @"Nerf";
        nerfgun.price = 39.99;
        nerfgun.info = @"Nerf Blaster";
        nerfgun.image = [UIImage imageNamed:@"nerfblaster"];
        [self.toys addObject:nerfgun];
        
        Toy *potatohead = [[Toy alloc] init];
        potatohead.name = @"Mr. Potato Head";
        potatohead.brand = @"Toy Story";
        potatohead.price = 14.99;
        potatohead.info = @"Mr. Potato Head";
        potatohead.image = [UIImage imageNamed:@"potatohead"];
        [self.toys addObject:potatohead];
        
        Toy *link = [[Toy alloc] init];
        link.name = @"Link";
        link.brand = @"The Legend of Zelda";
        link.price = 15.95;
        link.info = @"Link";
        link.image = [UIImage imageNamed:@"link"];
        [self.toys addObject:link];

        //Store default items
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_toys];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"toys"];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.toys count];
}

- (IBAction)unwindToList:(UIStoryboardSegue *)segue {
    AddToyViewController *viewController = segue.sourceViewController;
    
    //Get the newly created toy and store it
    if (viewController.toy != nil) {
        [_toys addObject:viewController.toy];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_toys];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"toys"];
        
        //Refresh table UI
        [[self tableView] reloadData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listPrototypeCell" forIndexPath:indexPath];

    // Configure the cell...
    Toy *toy = [self.toys objectAtIndex:indexPath.row];
    
    //Create subviews
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0,
                                                                      0,
                                                                      CELL_IMAGE_WIDTH,
                                                                      CELL_IMAGE_HEIGHT)];
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(CELL_IMAGE_WIDTH + CELL_TEXT_OFFSET,
                                                              CELL_IMAGE_HEIGHT * 0,
                                                              [[UIScreen mainScreen] bounds].size.width - CELL_IMAGE_WIDTH,
                                                              CELL_TEXT_HEIGHT)];
    UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(CELL_IMAGE_WIDTH + CELL_TEXT_OFFSET,
                                                               CELL_IMAGE_HEIGHT * (1.0 / 3.0),
                                                               [[UIScreen mainScreen] bounds].size.width - CELL_IMAGE_WIDTH,
                                                               CELL_TEXT_HEIGHT)];
    UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(CELL_IMAGE_WIDTH + CELL_TEXT_OFFSET,
                                                              CELL_IMAGE_HEIGHT * (2.0 / 3.0),
                                                              [[UIScreen mainScreen] bounds].size.width - CELL_IMAGE_WIDTH,
                                                              CELL_TEXT_HEIGHT)];
    
    //Configure subviews
    [name setFont:[UIFont boldSystemFontOfSize:CELL_TEXT_TITLE_SIZE]];
    [date setFont:[UIFont italicSystemFontOfSize:CELL_TEXT_SIZE]];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    //Populate subviews
    image.image = toy.image;
    name.text = toy.name;
    price.text = [@"$" stringByAppendingString:[[NSNumber numberWithFloat:toy.price] stringValue]];
    date.text = [@"Added: " stringByAppendingString:[dateFormatter stringFromDate:toy.createdDate]];
    
    //Add subviews to cell
    [cell addSubview:image];
    [cell addSubview:name];
    [cell addSubview:price];
    [cell addSubview:date];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedToy = [self.toys objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"toyDetailSegue" sender:tableView];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"toyDetailSegue"]) {
        ToyDetailViewController *viewController = [segue destinationViewController];
        [viewController setToy:self.selectedToy];
    }
}

@end
