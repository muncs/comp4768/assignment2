//
//  ToyTableViewController.h
//  assignment2
//
//  Created by Devin on 2018-10-25.
//  Copyright © 2018 Devin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ToyTableViewController : UITableViewController

- (IBAction)unwindToList:(UIStoryboardSegue *)segue;

@end

NS_ASSUME_NONNULL_END
