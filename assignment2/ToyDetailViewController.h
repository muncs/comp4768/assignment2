//
//  ToyDetailViewController.h
//  assignment2
//
//  Created by Devin on 2018-10-25.
//  Copyright © 2018 Devin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Toy.h"

NS_ASSUME_NONNULL_BEGIN

@interface ToyDetailViewController : UIViewController

@property Toy *toy;

-(void)setToyModel:(Toy*)toy;

@end

NS_ASSUME_NONNULL_END
