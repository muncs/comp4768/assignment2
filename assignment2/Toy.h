//
//  Toy.h
//  assignment2
//
//  Created by Devin on 2018-10-25.
//  Copyright © 2018 Devin. All rights reserved.
//

#ifndef Toy_h
#define Toy_h

#import <UIKit/UIKit.h>

@interface Toy : NSObject <NSCoding>

@property NSString *name;
@property NSString *brand;
@property float price;
@property UIImage *image;
@property NSString *info;
@property NSDate *createdDate;

@end

#endif /* Toy_h */
