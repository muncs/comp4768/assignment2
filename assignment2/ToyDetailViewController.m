//
//  ToyDetailViewController.m
//  assignment2
//
//  Created by Devin on 2018-10-25.
//  Copyright © 2018 Devin. All rights reserved.
//

#import "ToyDetailViewController.h"

@interface ToyDetailViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UITextField *textBrand;
@property (weak, nonatomic) IBOutlet UITextField *textPrice;
@property (weak, nonatomic) IBOutlet UITextView *textInfo;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end

@implementation ToyDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //Initialize the view with selected toy
    if (self.toy) {
        _textName.text = _toy.name;
        _textBrand.text = _toy.brand;
        _textPrice.text = [[NSNumber numberWithFloat:_toy.price] stringValue];
        _textInfo.text = _toy.info;
        _image.image = _toy.image;
    }
}

//Set the model
- (void)setToyModel:(Toy *)toy {
    self.toy = toy;
}

@end
