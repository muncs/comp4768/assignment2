//
//  AddToyViewController.h
//  assignment2
//
//  Created by Devin on 2018-10-25.
//  Copyright © 2018 Devin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Toy.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddToyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UITextField *textBrand;
@property (weak, nonatomic) IBOutlet UITextField *textPrice;
@property (weak, nonatomic) IBOutlet UITextView *textInfo;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonSave;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property Toy *toy;
- (IBAction)imageSelect:(UITapGestureRecognizer *)sender;

@end

NS_ASSUME_NONNULL_END
