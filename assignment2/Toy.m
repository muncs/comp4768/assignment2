//
//  Toy.m
//  assignment2
//
//  Created by Devin on 2018-10-25.
//  Copyright © 2018 Devin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Toy.h"

@implementation Toy

-(id)init {
    self = [super init];
    if (self) {
        self.createdDate = [[NSDate alloc] init];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.brand forKey:@"brand"];
    [encoder encodeObject:[NSNumber numberWithFloat:self.price] forKey:@"price"];
    [encoder encodeObject:self.createdDate forKey:@"createdDate"];
    [encoder encodeObject:self.info forKey:@"info"];
    [encoder encodeObject:self.image forKey:@"image"];
}

-(id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    self.name  = [decoder decodeObjectForKey:@"name"];
    self.brand = [decoder decodeObjectForKey:@"brand"];
    self.price  = [[decoder decodeObjectForKey:@"price"] floatValue];
    self.createdDate = [decoder decodeObjectForKey:@"createdDate"];
    self.info = [decoder decodeObjectForKey:@"info"];
    self.image = [decoder decodeObjectForKey:@"image"];
    
    return self;
}

@end
