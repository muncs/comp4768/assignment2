//
//  AddToyViewController.m
//  assignment2
//
//  Created by Devin on 2018-10-25.
//  Copyright © 2018 Devin. All rights reserved.
//

#import "AddToyViewController.h"

@interface AddToyViewController () <UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@end

@implementation AddToyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _image.userInteractionEnabled = YES;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if (sender != self.buttonSave) return;
    
    //Populate toy
    _toy = [[Toy alloc] init];
    _toy.name = _textName.text;
    _toy.brand = _textBrand.text;
    _toy.price = [_textPrice.text floatValue];
    _toy.info = _textInfo.text;
    _toy.image = _image.image;
}

- (IBAction)imageSelect:(UITapGestureRecognizer *)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    // Make sure the picker is notified when the user picks an image
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:true completion:nil];
}

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    _image.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
